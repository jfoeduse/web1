<?php
/**
 * Version 1 av datbasen...
 */

$sql[] = "
CREATE TABLE IF NOT EXISTS `db_version` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AUTO_INCREMENT=1;
";

$sql[] = "
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) COLLATE utf8_swedish_ci NOT NULL DEFAULT '',
  `last_name` varchar(50) COLLATE utf8_swedish_ci NOT NULL DEFAULT '',
  `email` varchar(50) COLLATE utf8_swedish_ci NOT NULL DEFAULT '',
  `password` varchar(50) COLLATE utf8_swedish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AUTO_INCREMENT=1 ;
";
