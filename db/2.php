<?php
/**
 * Version 2 av datbasen...
 */

$sql[] = "
CREATE TABLE IF NOT EXISTS `lists` (
`list_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `list_name` varchar(255) NOT NULL,
  PRIMARY KEY (`list_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
";

$sql[] = "
CREATE TABLE IF NOT EXISTS `user_list` (
`user_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `read` tinyint(1) NOT NULL,
  `write` tinyint(1) NOT NULL,
  `owner` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_list_id`),
  KEY `user_id` (`user_id`,`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
";

