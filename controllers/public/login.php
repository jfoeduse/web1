<?php

if (is_valid_form_id($_POST['form_build_id'])) {
  $query = "SELECT * FROM  users WHERE email = '" . trim($_POST['email']) . "' AND password = SHA1('" . SALT . trim($_POST['password']) . "')";
  $result = query($query);
  if (mysqli_num_rows($result) != 1) {
    set_message('Incorrect login details!!', 'warning');
    header('location: index.php');
    exit();
  }

  // Nu har vi en OK inloggning!
  // Sätter alla session variabler och går till index.php.
  $row = mysqli_fetch_assoc($result);
  $_SESSION['user_id'] = $row['user_id'];
  $_SESSION['first_name'] = $row['first_name'];
  $_SESSION['last_name'] = $row['last_name'];
  set_message('Welcome ' . $row['first_name'] . ' ' . $row['last_name'] . ' - you have successfully logged in!');
  header('location: index.php');
  exit();
}
