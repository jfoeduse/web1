<?php

if (is_valid_form_id($_POST['form_build_id'])) {
  $check_ok = TRUE;
  // Kollar igenom de krav som skall uppfyllas.
  if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    set_message('No valid Email', 'warning');
    $check_ok = FALSE;
  }
  if ($_POST['first_name'] == '') {
    set_message('No valid first_name', 'warning');
    $check_ok = FALSE;
  }
  if ($_POST['last_name'] == '') {
    set_message('No valid last_name', 'warning');
    $check_ok = FALSE;
  }
  if ($_POST['password'] == '') {
    set_message('No valid password', 'warning');
    $check_ok = FALSE;
  }
  if ($_POST['password'] != $_POST['password_again']) {
    set_message('password do not match', 'warning');
    $check_ok = FALSE;
  }

  // Kollar om användaren redan finns.
  $query = "SELECT * FROM users WHERE email = '" . $_POST['email'] . "'";
  $result = query($query);
  if (mysqli_num_rows($result) != 0) {
    set_message('Username already taken', 'danger');
    $check_ok = FALSE;
  }

  if ($check_ok) {
    $query = "INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `password`) VALUES (NULL, '" . $_POST['first_name'] . "', '" . $_POST['last_name'] . "', '" . $_POST['email'] . "', SHA1('" . SALT . trim($_POST['password']) . "'))";
    query($query);
    set_message('User ' . $_POST['first_name'] . ' ' . $_POST['last_name'] . ' with login: ' . $_POST['email'] . ' created!', 'success');
    header('location: index.php');
    exit;
  }
}
