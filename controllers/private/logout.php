<?php

if (is_valid_form_id($_POST['form_build_id'])) {
  if (session_destroy()) {
    session_name(SESSION_NAME);
    session_start();
    set_message('You have been successfully logged out!');
  }
  else {
    set_message('Error on logout!!!', 'danger');
  }
  header('location: index.php');
  exit();
}
