<?php
/**
 * script för att uppdatera databasen
 * Det måste finnas en underkatalog som heter db
 * I den skall det finnas en fil som heter codeversion.php
 * + en fil för varje databasversion
 */

include 'include/setup.php';

echo 'Update db körs...';

// Kollar aktuell kod version.
include 'db/codeversion.php';
echo "\nKodens version är: " . $code_version;


// Kollar om det finns någon databas.
$result = query("SHOW TABLES LIKE 'db_version'");
$n = mysqli_num_rows($result);

if ($n != 0) {
  // Hämtar senaste versionen
  $result = query("SELECT * FROM `db_version` ORDER BY id DESC");
  $row = mysqli_fetch_assoc($result);
  $db_version = $row['version'];
}
else {
  // Det finns ingen version av databasen.
  $db_version = 0;
}

echo "\nDatabasens version är: " . $db_version . "\n";

// Så länge databasen har lägre nummer än koden.
while ($db_version < $code_version) {
  $db_version++;
  if (file_exists('db/' . $db_version . '.php')) {
    unset($sql);
    echo "\nUpdaterar databasen till version: $db_version\n";
    include 'db/' . $db_version . '.php';
    if (isset($sql) && is_array($sql)) {
      foreach ($sql as $query) {
        $result = query($query);
      }
    }
    $result = query("INSERT INTO  `db_version` (`id` ,`version` ,`date`) VALUES (NULL ,  '" . $db_version . "', NOW())");
  }
  else {
    echo "\nERROR! Inte bra, filen till databasversion: $db_version saknas, exit...\n";
    exit;
  }
}
