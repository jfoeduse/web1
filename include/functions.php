<?php

function query($query) {
  global $link;
  if ($result = mysqli_query($link, $query)) {
    return $result;
  }
  else {
    set_message("Error in SQL-statement " . $query . mysqli_error($link), 'danger');
    return FALSE;
  }
}

function set_message($message, $type = 'info') {
  $types = ['success', 'info', 'warning', 'danger'];
  if (in_array($type, $types)) {
    $_SESSION['message'][$type][] = $message;
  }
  else {
    set_message($type . ' is unknown type', 'danger');
  }
}

function get_messages() {
  if (!empty($_SESSION['message'])) {
    foreach ($_SESSION['message'] as $key1 => $type) {
      if (!empty($type)) {
        foreach ($type as $message) {
          echo '<div class="alert alert-' . $key1 . ' fade in">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
          <strong>' . strtoupper($key1) . '!</strong> ' . $message . '
          </div>';
        }
      }
    }
  }
  unset($_SESSION['message']);
}

function generate_form_id() {
  $token = sha1(microtime() . SALT);
  $_SESSION['form_id'] = $token;
  return $token;
}

function is_valid_form_id($token) {
  return $_SESSION['form_id'] == $token;
}
