<fieldset>
  <h3>Create list</h3>
  <form method="post" action="index.php?c=create_list">
    <div class="form-group">
      <label>Listname</label>
      <input type="text" class="form-control" name="list_name" placeholder="Listname">
    </div>
    <input type="hidden" name="form_build_id" value="<?php echo generate_form_id(); ?>"><br><br>
    <button type="submit" class="btn btn-default btn-primary">Create</button>
  </form>
</fieldset>
<br>
<a class="btn btn-danger" href="index.php?v=show_my_lists">Cancel</a>
