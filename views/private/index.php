<fieldset>
  <legend>Logout</legend>
  <form method="post" action="index.php?c=logout">
    <div class="form-group">
      <input type="hidden" name="form_build_id" value="<?php echo generate_form_id(); ?>">
      <button type="submit" class="btn btn-default btn-primary">Logout</button>
    </div>
  </form>
</fieldset>

