<nav class="navbar navbar-default navbar-static-top">
  <div class="navbar-header">
    <button type="button" class="collapsed navbar-toggle" data-toggle="collapse"
            data-target="#navbar-1" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a href="index.php" class="navbar-brand">Home</a>
  </div>
  <div class="collapse navbar-collapse" id="navbar-1">
    <ul class="nav navbar-nav">
      <li><a class="active" href="index.php?v=show_my_lists">My lists</a></li>
      <li><a class="active" href="index.php?v=show_all_lists">All lists</a></li>
    </ul>
  </div>
</nav>
