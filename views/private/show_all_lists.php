<?php

$query = "SELECT *, lists.user_id as uid FROM lists INNER JOIN user_list 
on lists.list_id = user_list.list_id 
WHERE user_list.user_id = '" . $_SESSION['user_id'] . "'";
$result = query($query);

echo '<table class="table table-striped table-hover">           
<th>Lists</th><th></th><th></th>';
while ($row = mysqli_fetch_assoc($result)) {
  echo '
    <tr>
      <td><a href="index.php?v=show_tasks&&id=' . $row['list_id'] . '">' . $row['list_name'] . '</a></td>
      <td>';
  if ($_SESSION['user_id'] == $row['uid']) {
    echo '<a href="index.php?v=share_my_list&id=' . $row['list_id'] . '">share</a>';
  }
  echo '</td><td>';
  if ($_SESSION['user_id'] == $row['uid']) {
    echo '<a href="index.php?v=edit_list&id=' . $row['list_id'] . '">edit</a>';
  }
  echo '
      </td>
    </tr>';
}
echo '</table>';
