<?php


$posts = array(
  'email' => ['Email', 'email'],
  'first_name' => ['First name', 'text'],
  'last_name' => ['Last name', 'text'],
  'password' => ['Password', 'password'],
  'password_again' => ['Password again', 'password'],
);
?>

<fieldset>
  <h3>Register page</h3>
  <form method="post" action="index.php?c=register&v=register_form">
    <?php
    foreach ($posts as $post => $data) {
      if (!isset($_POST[$post])) {
        $_POST[$post] = '';
      }
      echo '
        <label>' . $data[0] . '</label>
        <input type="' . $data[1] . '" class="form-control" name="' . $post . '" value="' . $_POST[$post] . '" placeholder="' . $data[0] . '">';
    }
    ?>
    <input type="hidden" name="form_build_id" value="<?php echo generate_form_id(); ?>"><br>
    <button type="submit" class="btn btn-default btn-primary">Create</button>
  </form>
</fieldset>
<br><br>
<a href="index.php">Back</a>

