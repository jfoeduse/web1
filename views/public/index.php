<fieldset>
  <h3>Login page</h3>
  <form method="post" action="index.php?c=login">
    <div class="form-group">
      <label>Email</label>
      <input type="email" class="form-control" name="email" placeholder="Email">
    </div>
    <div class="form-group">
      <label>Password</label>
      <input type="password" class="form-control" name="password" placeholder="Password">
    </div>
    <input type="hidden" name="form_build_id" value="<?php echo generate_form_id(); ?>"><br><br>
    <button type="submit" class="btn btn-default btn-primary">Login</button>
  </form>
</fieldset>
<br>
<a href="index.php?c=register&v=register_form">Sign up</a>
