<?php
/*
 * Ändra inte i denna fil... inte till att börja med i varje fall.
 */

// Laddar det som behövs
include 'include/setup.php';

// Städar upp i $_GEt och sätter default värden om det inte finns något.
isset($_GET['c']) ? str_replace('.', '', $_GET['c']) : $_GET['c'] = 'index';
isset($_GET['v']) ? str_replace('.', '', $_GET['v']) : $_GET['v'] = 'index';

// Om det inte finns något värde på form_build_id så sätts det till FALSE.
!isset($_POST['form_build_id']) ? $_POST['form_build_id'] = FALSE : '';

// Avgör om man är inloggade eller inte.
empty($_SESSION['user_id']) ? $type = 'public' : $type = 'private';

// Behandla data i rätt controller.
$controller = 'controllers/' . $type . '/' . $_GET['c'] . '.php';
if (file_exists($controller)) {
  include $controller;
}

// Visa HTML genom att ladda rätt view.
$view = 'views/' . $type . '/' . $_GET['v'] . '.php';
// Nav används i head.php.
$nav = 'views/' . $type . '/nav.php';

include 'views/head.php';
get_messages();
if (file_exists($view)) {
  include $view;
}
else {
  set_message('No view named:' . $_GET['v'], 'danger');
  get_messages();
}
include 'views/foot.php';
